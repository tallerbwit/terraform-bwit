
resource "aws_s3_bucket" "awsbwitdatabucket" {
  bucket  = "my-bwit-data-bucket"
  tags = {
    Name        = "My bucket"
    Environment = "dev"
  }
}
