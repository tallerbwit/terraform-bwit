resource "aws_s3_bucket" "awsbwittrgbucket" {
  bucket  = "my-bwit-trg-bucket"
  tags = {
    Name        = "My bucket"
    Environment = "dev"
  }
}

# Create the Cloud function triggered by a `Finalize` event on the bucket
resource "aws_lambda_function" "function" {
    package_type  = "Image"
    function_name = "functiontriggeronaws"
    description   = "My awesome lambda function"
    image_uri     = "${var.account_id}.dkr.ecr.${var.region}.amazonaws.com/${var.service}:${var.build_hash}"
    handler       = "main.handler"
    role          = aws_iam_role.iam_for_lambda.arn
    runtime       = "python3.8"  # of course changeable
    timeout       = 30


    # Dependencies are automatically inferred so these lines can be deleted
    depends_on = [
        aws_s3_bucket.awsbwitdatabucket,  # declared in `storage.tf`
        aws_iam_role_policy_attachment.lambda_logs,
        aws_cloudwatch_log_group.example,

    ]

}


##########################################################################################
# Adding S3 bucket as trigger to my lambda and giving the permissions
##########################################################################################

resource "aws_s3_bucket_notification" "aws-lambda-trigger" {
        bucket = "${aws_s3_bucket.awsbwittrgbucket.id}"
        lambda_function {
                    lambda_function_arn = "${aws_lambda_function.function.arn}"
                    events              = ["s3:ObjectCreated:*"]
                    filter_prefix       = ""
                    filter_suffix       = ".csv"
                    }
}

resource "aws_lambda_permission" "test" {
        statement_id  = "AllowS3Invoke"
        action        = "lambda:InvokeFunction"
        function_name = "${aws_lambda_function.function.function_name}"
        principal = "s3.amazonaws.com"
        source_arn = "arn:aws:s3:::${aws_s3_bucket.awsbwittrgbucket.id}"
}


# This is to optionally manage the CloudWatch Log Group for the Lambda Function.
# If skipping this resource configuration, also add "logs:CreateLogGroup" to the IAM policy below.
resource "aws_cloudwatch_log_group" "example" {
  name              = "/aws/lambda/${var.lambda_function_name}"
  retention_in_days = 14
}

