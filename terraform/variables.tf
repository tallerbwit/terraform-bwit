
variable "lambda_function_name" {
  default = "functiontriggeronaws"
}
variable "s3_bucket" {
  default = "my-bwit-trg-bucket"
}
variable "account_id" {
  default = "522308994965"
}
variable "region" {
  default = "us-east-1"
}
variable "service" {
  default = "iawsmodelpnd3"
}
variable "build_hash" {
  default = "vs29.0"
}

