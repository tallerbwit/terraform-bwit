''' import sys
import json
import boto3
import urllib.parse

#def handler(event, context): 
#    return 'Hello from AWS Lambda using Python' + sys.version + '!'
s3_client = boto3.client('s3')
def handler(event, context):
    return 'Hello from AWS Lambda using Python' + sys.version + '!'        
    print("Received event: " + json.dumps(event, indent=2))
    

    # Get the object from the event and show its content type
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    print("key:",key)
    print("bucket:",bucket)
    
    try:
        response = s3_client.get_object(Bucket=bucket, Key=key)
        print("CONTENT TYPE: " + response['ContentType'])
        return response['ContentType']
    except Exception as e:
        print(e)
        print('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
        raise e
 '''
import sys
import json
import urllib.parse
import boto3
import pandas as pd
import warnings
warnings.filterwarnings('error') 
from sklearn.ensemble import RandomForestClassifier
import io

s3_client = boto3.client('s3')

def handler(event, context):
    print("Received event: " + json.dumps(event, indent=2))
    print(pd.DataFrame())


    # Get the object from the event and show its content type
    bucket = event['Records'][0]['s3']['bucket']['name']
    key = urllib.parse.unquote_plus(event['Records'][0]['s3']['object']['key'], encoding='utf-8')
    print("key:",key)
    print("bucket:",bucket)
    try:
        response = s3_client.get_object(Bucket=bucket, Key=key)
        #print("CONTENT TYPE: " + response['ContentType'])
        #print("contenido:",response['Body'].read().decode('utf-8')) 

        # get object and file (key) from bucket
        feature_cols = ("CustomerId","TOB","Bal01","MaxDqBin01","MtgBal01","NonBankTradesDq01","FlagGB","FlagSample")
        df_train = pd.read_csv(response['Body'], names = feature_cols) # 'Body' is a key word
        print("initial_df:",df_train.dtypes)
        print("initial_df:",df_train.shape)

        # entrenando el codigo
        feature_cols_model = ["TOB","Bal01","MaxDqBin01","MtgBal01","NonBankTradesDq01"]

        X_train=df_train[feature_cols_model]
        Y_train=df_train[['FlagGB']]
        dt = RandomForestClassifier()
        dt.fit(X_train, Y_train)

        print("len(y_pred):",len(Y_train))

        # Predict test set labels 
        y_pred = dt.predict(X_train)
        df_train['predict']=y_pred

        print("initial_df:",df_train.shape)

        return response['ContentType']
    except Exception as e:
        print(e)
        print('Error getting object {} from bucket {}. Make sure they exist and your bucket is in the same region as this function.'.format(key, bucket))
        raise e