FROM public.ecr.aws/lambda/python:3.8

# Define global args
ARG LAMBDA_TASK_ROOT="/var/task"

# Copy function code
COPY main.py ${LAMBDA_TASK_ROOT}

# Install the function's dependencies using file requirements.txt
# from your project folder.
COPY requirements.txt  .

# Install the runtime interface client
RUN  pip3 install -r requirements.txt --target "${LAMBDA_TASK_ROOT}"

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD [ "main.handler" ]
